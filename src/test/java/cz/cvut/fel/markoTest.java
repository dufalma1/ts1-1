package cz.cvut.fel;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class markoTest {
    @Test
    public void testFactorial5(){
        marko mar = new marko();

        assertEquals(120,mar.factorial(5));
    }

    @Test
    public void testFactorial0(){
        marko mar = new marko();

        assertEquals(1,mar.factorial(0));
    }

    @Test
    public void factorialNegativeNumber(){
        marko mar = new marko();

        assertThrows(IllegalArgumentException.class, () -> mar.factorial(-10));

    }

    @Test
    public void testFactoriaOfl5(){
        marko mar = new marko();

        assertEquals(120,mar.factorialIterative(5));
    }

    @Test
    public void testFactoriaOfl0(){
        marko mar = new marko();

        assertEquals(1,mar.factorialIterative(0));
    }

    @Test
    public void factorialOfNegativeNumber(){
        marko mar = new marko();

        assertThrows(IllegalArgumentException.class, () -> mar.factorialIterative(-10));

    }
}