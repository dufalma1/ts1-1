package cz.cvut.fel;

public class marko {

    public long factorial(int n){
        if (n<0){
            throw new IllegalArgumentException("Factorial of negative number");
        }

        if (n==0){
            return 1;
        }
        return n*factorial(n-1);
    }

    public long factorialIterative(int n ) {
        if (n < 0) {
            throw new IllegalArgumentException("Mimo definiční obor");
        }
        long result = 1;
        for (int i = n; i > 1; i--) {
            result *= i;
        }
        return result;
    }

}
