package lab04;

import hw2.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ParametrisedTest {

    private Calculator calc = new Calculator();

    @BeforeEach
    public void init(){
        calc = new Calculator();
    }
    @ParameterizedTest(name = "{0} adds {1} shold be equal to {2}")
    @CsvSource({"1,2,3", "2,3,5", "1,1,3"})
    public void add_AddsAandB_returnC(int a, int b, int c){
        Assertions.assertEquals(c, calc.add(a,b));
    }

    // Parametrizovane testy na calc multiply
    @ParameterizedTest(name = "{0} times {1} shold be equal to {2}")
    @CsvSource({"1,2,2", "2,3,6", "1,0,0"})
    public void times_AtimesB_returnC(int a, int b, int c){
        Assertions.assertEquals(c, calc.multiply(a,b));
    }
}
